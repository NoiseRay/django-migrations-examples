from django.apps import AppConfig


class DjangomigrationsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'djangomigrations'

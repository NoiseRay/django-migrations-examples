# Generated by Django 3.2.6 on 2021-08-05 05:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('djangomigrations', '0003_auto_20210804_1954'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='age',
            field=models.CharField(default=10, max_length=30),
            preserve_default=False,
        ),
    ]
